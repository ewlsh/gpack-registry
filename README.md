# gpack-registry

> Hosted at registry.gpack.app

## Get Module Information

Send a GET request to `https://registry.gpack.app/x/package_name@version`

## Add Module

Add an entry to `packs.json` with the following:

```json
    {
        "name": "$reponame",
        "meta": {
            "$version": {
                "owner": "$repoowner",
                "host": "$repohost"
            }
        }
    }
```

$repohost - github, gnome, or gitlab
$repoowner - the repository owner (organization or user)
$reponame - the name of the repository
$version - the version to apply this location to, "*" to apply to all.
$name - the package name
