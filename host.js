const packs = require("./packs.json");

const packmap = new Map();

packs.forEach((pack) => {
    packmap.set(pack.name, pack);
});

const express = require("express");
const app = express();

const x = express.Router();

x.get('/:name@:version', (req, res) => {
    const packName = req.params.name || null;
    const packVersion = req.params.version || null;
    const pack = packmap.get(packName);

    if (!pack) {
        return res.status(404).send("Pack not found!");
    }

    if (!packVersion) {
        return res.status(401).send("Invalid pack version.");
    }

    let meta = pack.meta[packVersion];

    if (!meta) {
        // TODO Support partial meta work?
        meta = pack.meta["*"];
    }

    res.status(200).type('text/plain; charset=utf-8').send(JSON.stringify({ name: pack.name, owner: meta.owner, repo: meta.host }));
});

app.use('/x', x);

app.use((_, res) => res.status(404).send());

app.listen(process.env.PORT || 8181, () => {
    console.log("Started!");
});